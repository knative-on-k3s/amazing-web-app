const http = require('http')
const port = 8080

let index_page = `
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <style>
        body {
          background-color: ${process.env.BACKGROUND_COLOR};
        }
        h1 { 
          font-family: "Source Sans Pro"; font-weight: 300; font-size: 100px;
          display: block;
          color: #${process.env.COLOR};
        }
    </style>
  </head>
  <body>
    <h1>
      ${process.env.MESSAGE}
    </h1>   
  </body>
</html>  
`

const requestHandler = (request, response) => {
  response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
  response.end(index_page)
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('😡 something bad happened', err)
  }
  console.log(`🌍 server is listening on ${port}`)
})
