# amazing-web-app

## Build

```bash
echo ${docker_pwd} | docker login --username ${docker_user} --password-stdin

docker build -t ${docker_user}/${service} .
docker push ${docker_user}/${service}
```

## Deploy

```bash
kubectl apply --filename service.yaml

kubectl get ksvc

echo "http://${service}.${namespace}.${IP}.xip.io"
```
